package com.example.g13webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G13webserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(G13webserviceApplication.class, args);
	}

}
